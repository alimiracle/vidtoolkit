from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip, ImageClip

class VideoWatermarking:
    """
    A class for adding watermarks to videos.
    """

    def __init__(self, video_path):
        """
        Initialize the VideoWatermarking object with the path to the input video.

        Parameters:
            video_path (str): The path to the input video file.
        """
        self.video_path = video_path

    def add_text_watermark(self, text, position=(10, 10), fontsize=30, color='white', font='Arial', align='West', duration=None, output_path=None):
        """
        Add a text watermark to the video.

        Parameters:
            text (str): The text to be used as the watermark.
            position (tuple): The position of the watermark on the video frame, specified as (x, y) coordinates.
            fontsize (int): The font size of the text.
            color (str): The color of the text (e.g., 'white', 'red', '#00FF00').
            font (str): The font type of the text.
            align (str): The alignment of the text within the watermark area ('West', 'East', 'North', 'South', 'Center', 'Nord', 'Sud', 'Est', 'Ouest', 'center').
            duration (float): The duration of the watermark in seconds. If None, the watermark will be applied for the entire duration of the video.
            output_path (str): The path to save the watermarked video. If None, a default name will be used.

        Returns:
            str: The path to the watermarked video file.
        """
        try:
            # Load the video clip
            video_clip = VideoFileClip(self.video_path)

            # Create a TextClip with the specified text
            text_clip = TextClip(text, fontsize=fontsize, color=color, font=font, align=align)

            # Set the position and duration of the text watermark
            text_clip = text_clip.set_position(position).set_duration(duration or video_clip.duration)

            # Composite the text clip onto the video clip
            watermarked_clip = CompositeVideoClip([video_clip, text_clip])

            # Set the output path if provided, otherwise use a default name
            output_path = output_path or 'watermarked_video.mp4'

            # Write the watermarked video to the output file
            watermarked_clip.write_videofile(output_path, codec='libx264', audio_codec='aac')

            return output_path

        except Exception as e:
            print(f"An error occurred while adding text watermark: {str(e)}")
            return None

    def add_image_watermark(self, image_path, position=(10, 10), duration=None, output_path=None):
        """
        Add an image watermark to the video.

        Parameters:
            image_path (str): The path to the image file to be used as the watermark.
            position (tuple): The position of the watermark on the video frame, specified as (x, y) coordinates.
            duration (float): The duration of the watermark in seconds. If None, the watermark will be applied for the entire duration of the video.
            output_path (str): The path to save the watermarked video. If None, a default name will be used.

        Returns:
            str: The path to the watermarked video file.
        """
        try:
            # Load the video clip
            video_clip = VideoFileClip(self.video_path)

            # Load the image watermark
            image_clip = ImageClip(image_path)

            # Set the position and duration of the image watermark
            image_clip = image_clip.set_position(position).set_duration(duration or video_clip.duration)

            # Composite the image clip onto the video clip
            watermarked_clip = CompositeVideoClip([video_clip, image_clip])

            # Set the output path if provided, otherwise use a default name
            output_path = output_path or 'watermarked_video.mp4'

            # Write the watermarked video to the output file
            watermarked_clip.write_videofile(output_path, codec='libx264', audio_codec='aac')

            return output_path

        except Exception as e:
            print(f"An error occurred while adding image watermark: {str(e)}")
            return None
