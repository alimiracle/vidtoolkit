from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip
from moviepy.video.tools.subtitles import SubtitlesClip

class VideoSubtitle:
    """Class for adding subtitles to videos."""

    def __init__(self, video_path):
        """
        Initialize VideoSubtitle object.

        Parameters:
            video_path (str): The path to the input video file.
        """
        self.video_path = video_path

    def add_subtitle(self, subtitle_text, start_time, end_time, position=(10, 10), fontsize=20, color='white', font='Arial', align='West', output_path=None):
        """
        Add subtitle text to the video.

        Parameters:
            subtitle_text (str): The text to be used as the subtitle.
            start_time (float): The start time of the subtitle in seconds.
            end_time (float): The end time of the subtitle in seconds.
            position (tuple): The position of the subtitle on the video frame, specified as (x, y) coordinates.
            fontsize (int): The font size of the subtitle text.
            color (str): The color of the subtitle text (e.g., 'white', 'red', '#00FF00').
            font (str): The font type of the subtitle text.
            align (str): The alignment of the subtitle text within the subtitle area ('West', 'East', 'North', 'South', 'Center', 'Nord', 'Sud', 'Est', 'Ouest', 'center').
            output_path (str, optional): The path to save the video with subtitles. If not provided, a default name will be used.

        Returns:
            str: The path to the video with subtitles.
        """
        try:
            # Load the video clip
            video_clip = VideoFileClip(self.video_path)

            # Create a TextClip with the specified subtitle text
            subtitle_clip = TextClip(subtitle_text, fontsize=fontsize, color=color, font=font, align=align)

            # Set the position and duration of the subtitle
            subtitle_clip = subtitle_clip.set_position(position).set_duration(end_time - start_time).set_start(start_time)

            # Composite the subtitle clip onto the video clip
            video_with_subtitle = CompositeVideoClip([video_clip, subtitle_clip])

            # Set the output path if provided, otherwise use a default name
            output_path = output_path or 'video_with_subtitle.mp4'

            # Write the video with subtitles to the output file
            video_with_subtitle.write_videofile(output_path, codec='libx264', audio_codec='aac')

            return output_path

        except Exception as e:
            print(f"An error occurred while adding subtitles: {str(e)}")
            return None

    def add_subtitle_from_srt(self, srt_path, output_path=None):
        """
        Add subtitles to the video from an SRT file.

        Parameters:
            srt_path (str): The path to the SRT file containing subtitles.
            output_path (str, optional): The path to save the video with subtitles. If not provided, a default name will be used.

        Returns:
            str: The path to the video with subtitles.
        """
        try:
            # Load the video clip
            video_clip = VideoFileClip(self.video_path)

            # Load the subtitles from the SRT file
            subtitles = SubtitlesClip(srt_path)

            # Overlay the subtitles onto the video clip
            video_with_subtitles = CompositeVideoClip([video_clip, subtitles.set_position(('center', 'bottom'))])

            # Set the output path if provided, otherwise use a default name
            output_path = output_path or 'video_with_subtitles.mp4'

            # Write the video with subtitles to the output file
            video_with_subtitles.write_videofile(output_path, codec='libx264', audio_codec='aac')

            return output_path

        except Exception as e:
            print(f"An error occurred while adding subtitles: {str(e)}")
            return None
