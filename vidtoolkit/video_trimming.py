import os
from moviepy.editor import VideoFileClip

class VideoTrimming:
    """Class for trimming video clips."""

    def __init__(self, filename):
        self.filename = filename

    def extract_frames(self, start_time, end_time, with_audio=True, output_filename=None):
        try:
            name, ext = os.path.splitext(self.filename)
            output_name = output_filename if output_filename else name + "_extracted.mp4"
            if not os.path.exists(self.filename):
                print('Video file not found.')
                return None
            clip = VideoFileClip(self.filename)
            if start_time < 0 or start_time > clip.duration or end_time < 0 or end_time > clip.duration or end_time <= start_time:
                print('Invalid time parameters.')
                return None
            subclip = clip.subclip(start_time, end_time).without_audio() if not with_audio else clip.subclip(start_time, end_time)
            if with_audio:
                audio = clip.audio
                subclip = subclip.set_audio(audio)
            subclip.write_videofile(output_name)
            return output_name
        except Exception as e:
            print('An error occurred: ' + str(e))
            return None
